import { Schema, model } from 'mongoose'

// Defining the user schema
const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

// Exporting it as a model
export default model('User', UserSchema)
