import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

// Connection to MongoDB endpoint
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

// API endpoint initialization
const app = express()
// Enable CORS
app.use(cors())
// Parse urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }))
// Parse json bodies
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

// Get list of all users
app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

// Get user by id
app.get('/users/:id', (req, res) => {
  UserModel.findById(req.params.id, (err, result) => {
    if (err) res.send(err)
    else res.send(result)
  })
})

// Update user by id
app.put('/users/:id', (req, res) => {
  // Supposing the request body is already well formatted
  let update_query = req.body
  console.log(update_query)
  // This function updates the user entry in the db by its Id and updates it
  UserModel.findByIdAndUpdate(req.params.id, update_query, (err, result) => {
    if (err) res.send(err)
    else res.send(result)
  })
})

// Add new user
app.post('/users', (req, res) => {
  // Inizializing a new user model
  let user = new UserModel()

  // Preparing the new user model attributes
  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  // Save the new user
  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

// Delete user by id
app.delete('/users/:id', (req, res) => {
  // This function finds the db entry by Id and deletes it
  UserModel.findByIdAndDelete(req.params.id, (err, result) => {
    res.send(result)
  })
})

// Exposing API endpoint
app.listen(8080, () => console.log('Example app listening on port 8080!'))
